class Unafold < Formula
  homepage "http://mfold.rna.albany.edu/"
# The mfold site no longer allows download of unafold-3.8.tar.gz
  url "http://rnaspace.sourceforge.net/software/unafold-3.8.tar.gz"
  sha256 "f9392621762a74b02eb026caceb69568ea65b0cf0891ba25a805da721a90a275"

  depends_on "gd"
  depends_on "gnuplot"

  def install
# 	sed \
#		-e 's:hybrid (UNAFold) 3.7:hybrid (UNAFold) 3.8:g' \
#		-i tests/hybrid.tml || die
    system "./configure", "--disable-debug", "--disable-dependency-tracking",
                          "--prefix=#{prefix}"
    system "make"
#    Download the XML::Parser from cpan.org to do  make check (but check is useless See test/README)
#    https://stackoverflow.com/questions/9693031/how-to-install-xmlparser-without-expat-devel
#    system "make", "check"
    system "make", "install"
  end
end
