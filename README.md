### homebrew-chem

Hargrove-group's public `aeh-group/chem` homebrew tap and project space visible to the world (public="he project can be accessed without any authentication."

#### How do I install these formulae?

`brew tap aeh-group/homebrew-chem https://gitlab.oit.duke.edu/aeh-group/homebrew-chem.git`

`brew install aeh-group/chem/<formula>`

 or  `brew install <formula>` replacing <formula> with the name of *.rb file in Formula (without the .rb)
 
 Note: Even for software with no formual,  the `brew link` command can be used to setup paths to build and run it. For example, see this snippet: ["rnastructure howto install and use bootcamp examples"](https://gitlab.oit.duke.edu/snippets/179) (which should work on macos, linux, or windows with WSL/WSL2).

TODO: create fomulas for hard-to-find software source tarballs in files folder at other homebrew2 project:
https://gitlab.oit.duke.edu/aeh-group/public-project/-/tree/master/files

#### Documentation

Pre-requisites: you must first install `brew` on macos, linux, or inside WSL/WSL2 on windows per: https://docs.brew.sh/Installation

Once brew is installed:
`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
